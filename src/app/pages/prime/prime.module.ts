import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimeComponent } from 'src/app/pages/prime/prime.component';
import { PrimeRoutingModule } from './prime-routing.module';


@NgModule({
  declarations: [PrimeComponent],
  imports: [
    CommonModule,
    PrimeRoutingModule
  ]
})
export class PrimeModule { }
