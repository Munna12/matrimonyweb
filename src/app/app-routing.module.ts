import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { } from 'src/app/pages/prime/prime.component';


const routes: Routes = [
  { path: '',  loadChildren: () => import('src/app/pages/home/home/home.module').then(m => m.HomeModule)},
  { path: 'prime',  loadChildren: () => import('src/app/pages/prime/prime.module').then(m => m.PrimeModule)},
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
